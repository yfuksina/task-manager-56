package ru.tsc.fuksina.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25645";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "562437824";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "127.0.0.1";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_SQL_DIALECT_KEY = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String CACHE_SECOND_LEVEL = "cache.second_level";

    @NotNull
    private static final String CACHE_CONFIG = "cache.config";

    @NotNull
    private static final String CACHE_FACTORY = "cache.factory";

    @NotNull
    private static final String CACHE_QUERY = "cache.query";

    @NotNull
    private static final String CACHE_MINIMAL_PUTS = "cache.minimal_puts";

    @NotNull
    private static final String CACHE_REGION_PREFIX = "cache.region_prefix";


    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(envKey)) return System.getProperty(envKey);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    public @NotNull String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DATABASE_USERNAME_KEY);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseSqlDialect() {
        return getStringValue(DATABASE_SQL_DIALECT_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO_KEY);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY);
    }

    @NotNull
    @Override
    public String getUseSecondLevelCache() {
        return getStringValue(CACHE_SECOND_LEVEL);
    }

    @Override
    public @NotNull String getCacheProviderConfig() {
        return getStringValue(CACHE_CONFIG);
    }

    @Override
    public @NotNull String getCacheFactoryClass() {
        return getStringValue(CACHE_FACTORY);
    }

    @Override
    public @NotNull String getUseQueryCache() {
        return getStringValue(CACHE_QUERY);
    }

    @Override
    public @NotNull String getUseMinimalPuts() {
        return getStringValue(CACHE_MINIMAL_PUTS);
    }

    @Override
    public @NotNull String getCacheRegionPrefix() {
        return getStringValue(CACHE_REGION_PREFIX);
    }

}
