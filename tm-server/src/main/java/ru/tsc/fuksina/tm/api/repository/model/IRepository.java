package ru.tsc.fuksina.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    @Nullable
    M findOneById(@NotNull String id);

    void remove(@NotNull M model);

    @Nullable
    void removeById(@NotNull String id);

    void update(@NotNull M model);

    void clear();

    boolean existsById(String id);

    long getSize();

    @NotNull
    EntityManager getEntityManager();

}
