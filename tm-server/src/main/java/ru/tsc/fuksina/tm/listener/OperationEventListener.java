package ru.tsc.fuksina.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.api.service.ISenderService;
import ru.tsc.fuksina.tm.dto.event.OperationEvent;

import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@Component
@NoArgsConstructor
public final class OperationEventListener implements Consumer<OperationEvent> {

    @NotNull
    @Autowired
    private ObjectMapper objectMapper;

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    @Autowired
    private ISenderService service;

    @Override
    @SneakyThrows
    public void accept(@NotNull final OperationEvent operationEvent) {
        @NotNull final Class entityClass = operationEvent.getEntity().getClass();
        if (!entityClass.isAnnotationPresent(Table.class)) return;
        @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
        @NotNull final Table table = (Table) annotation;
        operationEvent.setTable(table.name());
        @NotNull final String json = objectWriter.writeValueAsString(operationEvent);
        executorService.submit(() -> service.send(json));
    }

}
