package ru.tsc.fuksina.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.api.repository.model.IProjectRepository;
import ru.tsc.fuksina.tm.api.service.model.IProjectTaskService;
import ru.tsc.fuksina.tm.api.service.model.ITaskService;
import ru.tsc.fuksina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.fuksina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.fuksina.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.TaskIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;
import ru.tsc.fuksina.tm.model.Project;
import ru.tsc.fuksina.tm.model.Task;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskService.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        @Nullable final Project project = projectRepository.findOneById(projectId);
        task.setProjectId(project);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskService.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        tasks.stream()
                .forEach(task -> taskService.removeById(userId, task.getId()));
        projectRepository.removeById(userId, projectId);
    }

}
