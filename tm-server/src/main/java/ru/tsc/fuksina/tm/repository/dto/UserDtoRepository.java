package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.api.repository.dto.IUserDtoRepository;
import ru.tsc.fuksina.tm.dto.model.UserDto;

import java.util.List;

@Repository
@Scope("prototype")
public class UserDtoRepository extends AbstractRepositoryDto<UserDto> implements IUserDtoRepository {

    @NotNull
    @Override
    public List<UserDto> findAll() {
        return entityManager.createQuery("SELECT m  FROM UserDTO m", UserDto.class).getResultList();
    }

    @Nullable
    @Override
    public UserDto findOneById(@NotNull final String id) {
        return entityManager.find(UserDto.class, id);
    }

    @Nullable
    @Override
    public UserDto findOneByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT m FROM UserDTO m WHERE m.login = :login", UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDto findOneByEmail(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT m FROM UserDTO m WHERE m.email = :email", UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(UserDto.class, id));
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.createQuery("DELETE FROM UserDTO m WHERE m.login = :login", UserDto.class)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM UserDTO").executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM UserDTO m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM UserDTO m", Long.class)
                .getSingleResult();
    }

}
