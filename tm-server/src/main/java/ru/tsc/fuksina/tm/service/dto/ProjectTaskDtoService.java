package ru.tsc.fuksina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.fuksina.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.fuksina.tm.dto.model.TaskDto;
import ru.tsc.fuksina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.fuksina.tm.exception.entity.TaskNotFoundException;
import ru.tsc.fuksina.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.TaskIdEmptyException;
import ru.tsc.fuksina.tm.exception.field.UserIdEmptyException;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDto task = Optional.ofNullable(taskService.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        Optional.ofNullable(taskId).filter(item -> !item.isEmpty()).orElseThrow(TaskIdEmptyException::new);
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final TaskDto task = Optional.ofNullable(taskService.findOneById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).filter(item -> !item.isEmpty()).orElseThrow(ProjectIdEmptyException::new);
        @NotNull final List<TaskDto> tasks = taskService.findAllByProjectId(userId, projectId);
        tasks.stream()
                .forEach(task -> taskService.removeById(userId, task.getId()));
        projectService.removeById(userId, projectId);
    }

}
