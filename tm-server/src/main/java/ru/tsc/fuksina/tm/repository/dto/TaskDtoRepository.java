package ru.tsc.fuksina.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.api.repository.dto.ITaskDtoRepository;
import ru.tsc.fuksina.tm.dto.model.TaskDto;
import ru.tsc.fuksina.tm.enumerated.Sort;

import java.util.Collections;
import java.util.List;

@Repository
@Scope("prototype")
public class TaskDtoRepository extends AbstractUserOwnedRepositoryDto<TaskDto> implements ITaskDtoRepository {

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        return entityManager.createQuery("SELECT m  FROM TaskDTO m", TaskDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT m  FROM TaskDTO m WHERE m.userId = :userId", TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDto> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty() || projectId.isEmpty()) return Collections.emptyList();
        return entityManager
                .createQuery("SELECT m  FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId", TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull final String id) {
        return entityManager.find(TaskDto.class, id);
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id", TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(TaskDto.class, id));
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM TaskDTO").executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM TaskDTO m WHERE m.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(m) = 1 FROM TaskDTO m WHERE m.id = :id AND m.userId = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM TaskDTO m", Long.class)
                .getSingleResult();
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(m) FROM TaskDTO m WHERE m.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
