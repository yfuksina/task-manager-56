package ru.tsc.fuksina.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.fuksina.tm.api.service.IAuthService;
import ru.tsc.fuksina.tm.dto.request.AbstractUserRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.exception.system.AccessDeniedException;
import ru.tsc.fuksina.tm.dto.model.SessionDto;


import java.util.Optional;

@Getter
@Controller
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    protected SessionDto check (@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        Optional.ofNullable(request).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(role).orElseThrow(AccessDeniedException::new);
        @Nullable final String token = request.getToken();
        @NotNull SessionDto session = getAuthService().validateToken(token);
        Optional.ofNullable(session.getRole()).orElseThrow(AccessDeniedException::new);
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected SessionDto check (@Nullable final AbstractUserRequest request) {
        Optional.ofNullable(request).orElseThrow(AccessDeniedException::new);
        @Nullable final String token = request.getToken();
        Optional.ofNullable(token).filter(item -> !item.isEmpty()).orElseThrow(AccessDeniedException::new);
        return getAuthService().validateToken(token);
    }

}
