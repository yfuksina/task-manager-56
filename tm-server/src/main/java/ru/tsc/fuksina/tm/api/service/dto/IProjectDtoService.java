package ru.tsc.fuksina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.ProjectDto;
import ru.tsc.fuksina.tm.enumerated.Status;

public interface IProjectDtoService extends IUserOwnedServiceDto<ProjectDto> {

    @NotNull
    ProjectDto changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
