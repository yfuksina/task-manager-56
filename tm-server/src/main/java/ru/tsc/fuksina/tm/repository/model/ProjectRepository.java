package ru.tsc.fuksina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.api.repository.model.IProjectRepository;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.model.Project;

import java.util.Collections;
import java.util.List;

@Repository
@Scope("prototype")
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT m  FROM Project m", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        return entityManager.createQuery("SELECT m FROM Project m WHERE m.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        return entityManager
                .createQuery("SELECT m FROM Project m WHERE m.user.id = :userId AND m.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(Project.class, id));
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void clear() {
        @NotNull final List<Project> projects = findAll();
        projects.forEach(this::remove);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAll(userId);
        projects.forEach(this::remove);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.createQuery("SELECT COUNT(m) = 1 FROM Project m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(m) = 1 FROM Project m WHERE m.id = :id AND m.user.id = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(m) FROM Project", Long.class)
                .getSingleResult();
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(m) FROM Project m WHERE m.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
