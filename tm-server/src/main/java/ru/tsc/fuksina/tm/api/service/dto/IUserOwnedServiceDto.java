package ru.tsc.fuksina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.AbstractUserOwnedModelDto;
import ru.tsc.fuksina.tm.enumerated.Sort;

import java.util.Date;
import java.util.List;

public interface IUserOwnedServiceDto<M extends AbstractUserOwnedModelDto> extends IServiceDto<M> {

    @NotNull
    M create(@Nullable String userId, @Nullable String name);

    @NotNull
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateStart,
            @Nullable Date dateEnd
    );

    @NotNull
    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void add(@Nullable String userId, @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    void remove(@NotNull String userId, @NotNull M model);

    void removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    long getSize(@NotNull String userId);

    void update(@Nullable String userId, @NotNull M model);

    boolean existsById(@NotNull String userId, @NotNull String id);

}
