package ru.tsc.fuksina.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.api.service.*;
import ru.tsc.fuksina.tm.endpoint.*;
import ru.tsc.fuksina.tm.listener.EntityListener;
import ru.tsc.fuksina.tm.listener.OperationEventListener;
import ru.tsc.fuksina.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private Backup backup;

    public void run() {
        initPID();
        initJMS();
        initEndpoints();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutDown));
        backup.start();
    }

    private void prepareShutDown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN**");
        backup.stop();
    }


    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name+ "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initJMS() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new OperationEventListener());
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }


}
