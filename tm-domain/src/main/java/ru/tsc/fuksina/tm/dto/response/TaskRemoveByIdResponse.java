package ru.tsc.fuksina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.TaskDto;

@NoArgsConstructor
public final class TaskRemoveByIdResponse extends AbstractTaskResponse {

    public TaskRemoveByIdResponse(@Nullable final TaskDto task) {
        super(task);
    }

}
