package ru.tsc.fuksina.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.TaskDto;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<TaskDto> tasks;

    public TaskListResponse(@Nullable final List<TaskDto> tasks) {
        this.tasks = tasks;
    }

}
