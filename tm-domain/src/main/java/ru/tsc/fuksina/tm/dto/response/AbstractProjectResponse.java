package ru.tsc.fuksina.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.ProjectDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable ProjectDto project;

}
