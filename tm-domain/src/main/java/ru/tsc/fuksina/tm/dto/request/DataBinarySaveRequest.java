package ru.tsc.fuksina.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable final String token) {
        super(token);
    }

}
