package ru.tsc.fuksina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataJsonJaxbSaveRequest;
import ru.tsc.fuksina.tm.enumerated.Role;

@Component
public final class DataJsonJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE DATA IN JSON FILE]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonJaxbSaveRequest(getToken()));
    }

}
