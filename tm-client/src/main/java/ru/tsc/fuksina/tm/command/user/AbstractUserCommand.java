package ru.tsc.fuksina.tm.command.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.fuksina.tm.api.endpoint.IUserEndpoint;
import ru.tsc.fuksina.tm.command.AbstractCommand;
import ru.tsc.fuksina.tm.exception.entity.UserNotFoundException;
import ru.tsc.fuksina.tm.dto.model.UserDto;

@Getter
@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDto user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
