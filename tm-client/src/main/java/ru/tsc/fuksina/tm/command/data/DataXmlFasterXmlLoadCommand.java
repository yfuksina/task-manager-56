package ru.tsc.fuksina.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataXmlFasterXmlLoadRequest;
import ru.tsc.fuksina.tm.dto.request.UserLogoutRequest;
import ru.tsc.fuksina.tm.enumerated.Role;

@Component
public final class DataXmlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD DATA FROM XML FILE]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlFasterXmlLoadRequest(getToken()));
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

}
