package ru.tsc.fuksina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.ProjectListRequest;
import ru.tsc.fuksina.tm.dto.response.ProjectListResponse;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.dto.model.ProjectDto;
import ru.tsc.fuksina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT:]");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken(), sort);
        @NotNull final ProjectListResponse response = getProjectEndpoint().listProject(request);
        @Nullable final List<ProjectDto> projects = response.getProjects();
        int index = 1;
        for (ProjectDto project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

}
